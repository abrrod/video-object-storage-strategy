package com.adapter.example;

import java.io.OutputStream;

public interface FileRepository {

    public void saveFile(OutputStream stream, TypeOfFile type, String path);


    public enum TypeOfFile {

        STATIC (false), DEBUG(true), FAILED (true), EVIDENCE (false);

        private TypeOfFile(boolean isAsync) {
            this.isAsync = isAsync;
        }

        private boolean isAsync;

        public boolean isAsync() {
            return isAsync;
        }
    }
}
