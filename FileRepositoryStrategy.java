package com.adapter.example;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public class FileRepositoryStrategy implements FileRepository {

    private Map<String, FileRepository> repositoryMap;

    private List<String> enabledRepositories;

    public FileRepositoryStrategy(FileRepository filesystemrepository, FileRepository s3repository) {

        enabledRepositories.stream().forEach(i -> {

            switch (i) {
                case "S3": repositoryMap.put(i, s3repository)
                case "distribued": repositoryMap.put(i, filesystemrepository);
            }
        });
    }

    @Override
    public void saveFile(OutputStream stream, TypeOfFile type, String path) {

        repositoryMap.values().stream().forEach(fr -> {
            fr.saveFile(stream, type, path);
        });

    }
}
