package com.adapter.example;

import org.springframework.beans.factory.annotation.Value;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.OutputStream;
import java.util.Map;

public class S3FileRepositoryAdapter implements  FileRepository {

    Map<TypeOfFile, String> bucketsMapping;

    private S3Client s3client;

    private S3AsyncClient s3AsyncClient;

    public S3FileRepositoryAdapter(@Value("someproperty") String bucketFailed, @Value("someproperty") String bucketEvidence) {
        bucketsMapping.put(TypeOfFile.EVIDENCE, bucketEvidence);
        bucketsMapping.put(TypeOfFile.FAILED, bucketFailed);
    }

    @Override
    public void saveFile(OutputStream stream, TypeOfFile type, String path) {

        // Preparing previous actions like zipping?? --> use generic method


        // Create body and content independent of async or sync or which buckect you need to write to
        String bucket = bucketsMapping.get(type);
        PutObjectRequest putObjectRequest = null;
        Object request = getBody(type.isAsync());

        if (type.isAsync()) {
            //software.amazon.awssdk.services.s3.model
            s3AsyncClient.putObject(putObjectRequest, (AsyncRequestBody)request);
        } else {
            //software.amazon.awssdk.services.s3.model
            s3client.putObject(putObjectRequest, (RequestBody) request);
        }
    }

    private Object getBody(boolean isAsync) {
        return isAsync ? new AsyncRequestBody() : new RequestBody();

    }
}
